#include <vector>
#include <gsl/gsl_spline.h>
#include "FixedMesh.h"

class FixedMeshAnalytRotation : public FixedMesh {
 public:
  FixedMeshAnalytRotation(double,double,double,double,double,unsigned int);
  double beta(double x);
  double betap(double x);
  double uu(double x);
 private:
  const double beta0, // power law index of vphi(R)
    b, // turnover radius.
    nRotCurve; // sharpness of turnover
};
