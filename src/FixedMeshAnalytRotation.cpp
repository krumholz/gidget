#include <math.h>
#include <vector>
#include <iostream>

#include "FixedMesh.h"
#include "DiskUtils.h"
#include "FixedMeshAnalytRotation.h"

#include <gsl/gsl_sf_hyperg.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_integration.h>

double sign(double a) {
    if(a>0.0)
        return 1;
}

FixedMeshAnalytRotation :: 
FixedMeshAnalytRotation(double bet0, double turnoverRadius, 
			double nRC, double xm, double mst, unsigned int nnx):
  beta0(bet0), b(turnoverRadius),nRotCurve(nRC),FixedMesh(xm, mst, nnx)
{
  u1pbiPlusHalf[0] = uu(x(0.5)) * (1.0 + beta(x(0.5)));
  for(unsigned int n=1; n<=nxc; ++n) {
    u1pbiPlusHalf[n] = uu(xiPlusHalf[n])*(1.0+beta(xiPlusHalf[n]));
    uuv[n] = uu(xv[n]);
    betav[n] = beta(xv[n]);
    betapv[n] = betap(xv[n]);
  }


}

double FixedMeshAnalytRotation::uu(double x)
{
    return pow(1.0+pow(x/b,-fabs(nRotCurve*beta0)),-sign(beta0)/nRotCurve);
//    return 1.0/pow(pow(b/x,nRotCurve*beta0)+1.0,1.0/nRotCurve);
//  return pow(x,ip) / pow(pow(x,ip*soft)+bsf,1./soft);
}

double FixedMeshAnalytRotation::beta(double x)
{
  return beta0 - beta0/(1.0+pow(b/x,beta0*nRotCurve));
    
//  return bsf*ip/(bsf+pow(x,ip*soft));
}

double FixedMeshAnalytRotation::betap(double x)
{
    return - beta0*beta0*nRotCurve*pow(b/x, beta0*nRotCurve) / (x*pow(1.0+pow(b/x,beta0*nRotCurve),2.0));
//  return -bsf*ip*ip*soft*pow(x,-1.+ip*soft) / ((bsf+pow(x,ip*soft))*(bsf+pow(x,ip*soft)));
}






