#include <math.h>
#include <vector>
#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <gsl/gsl_bspline.h>
#include <gsl/gsl_multifit.h>

#include "FixedMesh.h"
#include "DiskUtils.h"
#include "FixedMeshTabRotation.h"
#include "Dimensions.h"

// Degree of B-spline fit; 5 = quartic, 6 = quintic
#define BSPLINE_DEGREE 6

// Number of breakpoints in the B spline
#define BSPLINE_BREAKPOINTS 15

// Number of B spline fit coefficients
#define BSPLINE_NCOEF (BSPLINE_BREAKPOINTS + BSPLINE_DEGREE - 2)

FixedMeshTabRotation :: 
FixedMeshTabRotation(std::string inputFileName, Dimensions *dim, 
		     double xm, double mst, 
		     unsigned int nnx): FixedMesh(xm, mst, nnx)
{
  // Open the input file
  std::ifstream rotFile;
  rotFile.open(inputFileName.c_str());

  // Bail out if we failed to open the file
  if (!rotFile.is_open()) {
    std::cerr << "Failed to open rotation curve file " << inputFileName << std::endl;
    exit(1);
  }

  // Read file once to count number of entries
  nEntries = 0;
  std::string entry;
  while (!rotFile.eof()) {
    std::getline(rotFile,entry);
    // Don't count empty lines or lines whose first character is #. To
    // check this, find first non-white space character and first
    // non-# character. If there are no non-whitespace characters,
    // skip. If the first non-whitespace character is #, skip.
    std::size_t first_non_whitespace = entry.find_first_not_of(" \t\n\v\f\r");
    if (first_non_whitespace == std::string::npos) continue;
    std::size_t first_comment = entry.find_first_of("#");
    if (first_non_whitespace == first_comment) continue;
    nEntries++;
  }

  // Allocate array to hold input rotation curve
  if (nEntries==0) {
    std::cerr << "File " << inputFileName << " appears to contain no data" 
	      << std::endl;
    exit(1);
  }
  rInput = new double[nEntries];
  vInput = new double[nEntries];

  // Now read data from beginning, then close
  rotFile.clear();
  rotFile.seekg(0, std::ios::beg);
  nEntries = 0;
  while (!rotFile.eof()) {
    std::getline(rotFile,entry);
    // Skip blank and comment lines
    std::size_t first_non_whitespace = entry.find_first_not_of(" \t\n\v\f\r");
    if (first_non_whitespace == std::string::npos) continue;
    std::size_t first_comment = entry.find_first_of("#");
    if (first_non_whitespace == first_comment) continue;
    // Read data
    std::stringstream entrystream(entry);
    entrystream >> rInput[nEntries] >> vInput[nEntries];
    nEntries++;
  }
  rotFile.close();

  // Construct normalized data. Note that quantities in the dimensions
  // object are in CGS units, while the input file is assumed to be in
  // kpc and km/s, so we need to do unit conversions. Also note that
  // we put this in a gsl_vector, since we're going to need it in that
  // form later.
  gsl_vector *logxInput = gsl_vector_alloc(nEntries);
  gsl_vector *uInput = gsl_vector_alloc(nEntries);
  for (int i=0; i<nEntries; i++) {
    gsl_vector_set(logxInput, i, log(rInput[i]/(dim->Radius/cmperkpc)));
    gsl_vector_set(uInput, i, vInput[i]*1.0e5/dim->vphiR);
  }

  // Allocate workspace for and associated stuff for B-spline solver
  wksp = gsl_bspline_alloc(BSPLINE_DEGREE, BSPLINE_BREAKPOINTS);
  dwksp = gsl_bspline_deriv_alloc(BSPLINE_DEGREE);
  B = gsl_vector_alloc(BSPLINE_NCOEF);
  dB = gsl_matrix_alloc(BSPLINE_NCOEF, 3);

  // Compute breakpoint locations using the method of Gans & Gill, Journal
  // of Applied Spectroscopy, 38, 297, 1984. The formula for
  // uniformly-spaced data is that two successive breakpoints t_i and t_i+1
  // should be separated by an amount such that
  // sum_{x=t_i}^{x=t_i+1} f(x)^0.5 dx = S_T
  // where
  // S_T = (1/(m+1)) sum f(x_i)^0.5 dx.
  // Here the first sum runs over all data points between t_i and
  // t_i+1, and the second runs all data points; m is the number of
  // knots, and f(x) is the function to be approximated. This formula
  // assumes uniform spacing dx, but here it is modified for
  // non-uniform data spacing such that dx is taken to be the mean
  // size of the data interval, averaging over the left and right
  // sides.
  double u0 = gsl_vector_get(uInput, 0);
  double x0 = gsl_vector_get(logxInput, 0);
  double x1 = gsl_vector_get(logxInput, 1);
  double bkpt_sum_target = sqrt(u0) * (x1-x0);
  for (int i=1; i<nEntries-1; i++) {
    u0 = gsl_vector_get(uInput, i);
    x0 = gsl_vector_get(logxInput, i-1);
    x1 = gsl_vector_get(logxInput, i+1);
    bkpt_sum_target += sqrt(u0) * 0.5 * (x1-x0);
  }
  u0 = gsl_vector_get(uInput, nEntries-1);
  x0 = gsl_vector_get(logxInput, nEntries-2);
  x1 = gsl_vector_get(logxInput, nEntries-1);
  bkpt_sum_target += sqrt(u0) * (x1-x0);
  bkpt_sum_target /= (BSPLINE_BREAKPOINTS + 1);
  gsl_vector *bkpt_vec = gsl_vector_alloc(BSPLINE_BREAKPOINTS);
  gsl_vector_set(bkpt_vec, 0, gsl_vector_get(logxInput, 0));
  gsl_vector_set(bkpt_vec, BSPLINE_BREAKPOINTS-1, 
		 gsl_vector_get(logxInput, nEntries-1));
  int ptr=0;
  for (int i=1; i<BSPLINE_BREAKPOINTS-1; i++) {
    double sum = 0;
    while (sum < bkpt_sum_target) {
      if (ptr == 0) {
	u0 = gsl_vector_get(uInput, 0);
	x0 = gsl_vector_get(logxInput, 0);
	x1 = gsl_vector_get(logxInput, 1);
	sum += sqrt(u0) * (x1-x0);
      } else if (ptr == nEntries-1) {
	u0 = gsl_vector_get(uInput, nEntries-1);
	x0 = gsl_vector_get(logxInput, nEntries-2);
	x1 = gsl_vector_get(logxInput, nEntries-1);
	sum += sqrt(u0) * (x1-x0);
      } else {
	u0 = gsl_vector_get(uInput, ptr);
	x0 = gsl_vector_get(logxInput, ptr-1);
	x1 = gsl_vector_get(logxInput, ptr+1);
	sum += sqrt(u0) * 0.5 * (x1-x0);
      }
      ptr++;
    }
    gsl_vector_set(bkpt_vec, i, gsl_vector_get(logxInput, ptr-1));
  }

  // Print diagnostic information
#ifdef PRINT_ROT_CURVE_FIT
  std::cout << "Placing b-spline breakpoints at:" << std::endl;
  for (int i=0; i<BSPLINE_BREAKPOINTS; i++) {
    std::cout << "     r = " << exp(gsl_vector_get(bkpt_vec, i))*dim->Radius/cmperkpc <<
      " kpc, x = " << exp(gsl_vector_get(bkpt_vec, i)) <<
      ", log_10 x = " << log10(exp(gsl_vector_get(bkpt_vec, i))) <<
      std::endl;
  }
#endif

  // Construct knots vector
  gsl_bspline_knots(bkpt_vec, wksp);

  // Allocate workspace for the least squares fit to find the B spline
  // coefficients; pred is the predictor variable matrix, cov is the
  // covariance matrix, coef is the vector of best-fitting
  // coefficients, and mw is the workspace used by the gsl fitter
  gsl_matrix *pred = gsl_matrix_alloc(nEntries, BSPLINE_NCOEF);
  gsl_multifit_linear_workspace *mw = 
    gsl_multifit_linear_alloc(nEntries, BSPLINE_NCOEF);
  cov = gsl_matrix_alloc(BSPLINE_NCOEF, BSPLINE_NCOEF);
  coef = gsl_vector_alloc(BSPLINE_NCOEF);

  // Load the predictor matrix with the B spline functions evaluated
  // at the input positions
  for (int i=0; i<nEntries; i++) {
    double logx = gsl_vector_get(logxInput, i); // Position of data point
    gsl_bspline_eval(logx, B, wksp); // Evaluate B splines at this
				  // position
    // Load predictor matrix
    for (int j=0; j<BSPLINE_NCOEF; j++) {
      double Bij = gsl_vector_get(B, j);
      gsl_matrix_set(pred, i, j, Bij);
    }
  }
  
  // Perform least squares fit to find coefficients
  double chisq;
  gsl_multifit_linear(pred, uInput, coef, cov, &chisq, mw);

  // Print diagnostic message
  std::cout << "Found B-spline fit to input rotation curve, chisq/dof = "
	    << chisq/(nEntries-BSPLINE_NCOEF) << std::endl;

  // Intialize xlast value
  xlast = -1.0;

  // Print out fit values and their derivatives
#ifdef PRINT_ROT_CURVE_FIT
  std::cout << "Fit results:" << std::endl;
  std::cout << "rInput [kpc]    vInput [km/s]    vFit [km/s]"
	    << "             x             u       betaFit   (dbeta/dlnx)Fit" << std::endl;
  for (int i=0; i<nEntries; i++) {
    double ufit, upfit, uppfit, err, beta, dbetadlnx;

    // Get log x value of this point
    double logx = gsl_vector_get(logxInput, i);

    // Evaluate B-splines and their derivatives, store in dB
    gsl_bspline_deriv_eval(logx, 2, dB, wksp, dwksp);

    // Copy each column of dB into B, then evaluate B-spline fit value
    gsl_matrix_get_col(B, dB, 0);
    gsl_multifit_linear_est(B, coef, cov, &ufit, &err);
    gsl_matrix_get_col(B, dB, 1);
    gsl_multifit_linear_est(B, coef, cov, &upfit, &err);
    gsl_matrix_get_col(B, dB, 2);
    gsl_multifit_linear_est(B, coef, cov, &uppfit, &err);

    // Get beta and beta'
    beta = upfit/ufit;
    dbetadlnx = (uppfit/ufit - beta*beta/(ufit*ufit));

    // Write output
    std::cout << std::setw(12) << rInput[i] 
	      << std::setw(17) << vInput[i]
	      << std::setw(15) << ufit*dim->vphiR/1.0e5
	      << std::setw(14) << rInput[i]/(dim->Radius/cmperkpc)
	      << std::setw(14) << vInput[i]/(dim->vphiR/1.0e5)
	      << std::setw(14) << beta 
	      << std::setw(18) << dbetadlnx << std::endl;
  }
#endif

  // Free unneeded memory
  gsl_matrix_free(pred);
  gsl_vector_free(bkpt_vec);
  gsl_vector_free(logxInput);
  gsl_vector_free(uInput);
  gsl_multifit_linear_free(mw);

  // Fill required array elements
  u1pbiPlusHalf[0] = uu(x(0.5)) * (1.0 + beta(x(0.5)));
  for(unsigned int n=1; n<=nxc; ++n) {
    u1pbiPlusHalf[n] = uu(xiPlusHalf[n])*(1.0+beta(xiPlusHalf[n]));
    uuv[n] = uu(xv[n]);
    betav[n] = beta(xv[n]);
    betapv[n] = betap(xv[n]);
  }


}

double FixedMeshTabRotation::uu(double x)
{
  double ufit, err;

  // Is this x different than xlast? If so, we need to recompute
  // B-spline values at the new position
  if (x != xlast) {
    gsl_bspline_deriv_eval(log(x), 2, dB, wksp, dwksp);
    xlast = x;
  }

  // Get the required colum from the dB matrix
  gsl_matrix_get_col(B, dB, 0);

  // Evaluate the B-spline
  gsl_multifit_linear_est(B, coef, cov, &ufit, &err);

  // Return
  return ufit;
}

double FixedMeshTabRotation::beta(double x)
{
  double ufit, upfit, err;

  // Is this x different than xlast? If so, we need to recompute
  // B-spline values at the new position
  if (x != xlast) {
    gsl_bspline_deriv_eval(log(x), 2, dB, wksp, dwksp);
    xlast = x;
  }

  // Evaluate the B spline estimate for u
  gsl_matrix_get_col(B, dB, 0);
  gsl_multifit_linear_est(B, coef, cov, &ufit, &err);

  // Same for u'
  gsl_matrix_get_col(B, dB, 1);
  gsl_multifit_linear_est(B, coef, cov, &upfit, &err);

  // Return
  return upfit/ufit;
}

double FixedMeshTabRotation::betap(double x)
{
  double beta, ufit, upfit, uppfit, err;

  // Is this x different than xlast? If so, we need to recompute
  // B-spline values at the new position
  if (x != xlast) {
    gsl_bspline_deriv_eval(log(x), 2, dB, wksp, dwksp);
    xlast = x;
  }

  // Evaluate the B spline estimate for u
  gsl_matrix_get_col(B, dB, 0);
  gsl_multifit_linear_est(B, coef, cov, &ufit, &err);

  // Same for u' and u''
  gsl_matrix_get_col(B, dB, 1);
  gsl_multifit_linear_est(B, coef, cov, &upfit, &err);
  gsl_matrix_get_col(B, dB, 2);
  gsl_multifit_linear_est(B, coef, cov, &uppfit, &err);

  // Compute value to return
  beta = upfit/ufit;
  return (uppfit/ufit - beta*beta/(ufit*ufit))/x;
}


// The destructor. This de-allocates all the various gsl stuff.
FixedMeshTabRotation :: 
~FixedMeshTabRotation() {
  gsl_vector_free(coef);
  gsl_vector_free(B);
  gsl_matrix_free(dB);
  gsl_matrix_free(cov);
  gsl_bspline_free(wksp);
  gsl_bspline_deriv_free(dwksp);
  delete rInput;
  delete vInput;
}


// This method dumps the interpolated rotation curve to a file for
// convenient access
void FixedMeshTabRotation::
dumpRotCurve(std::string filename, Dimensions *dim) {

  // Construct the output filename
  std::string outFileName = filename + "_rotCurve.txt";
  
  // Open the output file
  std::ofstream outFile;
  outFile.open(outFileName.c_str());
  
  // Write to file
  for (int i=1; i<xv.size(); i++)
    outFile << std::setw(14) << xv[i]
	    << std::setw(14) << xv[i]*dim->Radius/cmperkpc
	    << std::setw(14) << uuv[i]
	    << std::setw(14) << uuv[i]*dim->vphiR/1.0e5
	    << std::setw(14) << betav[i]
	    << std::setw(14) << betapv[i]*xv[i]
	    << std::endl;

  // Close file
  outFile.close();
}
