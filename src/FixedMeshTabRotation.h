#include <vector>
#include <gsl/gsl_bspline.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>

class Dimensions;
class FixedMesh;

/* This class handles tabulated rotation curves. It is intialiazed by
   passing in a file name, the Dimensions object (needed to convert
   the dimensional quantities in the input file to non-dimensional
   ones), as well as the standard paramters to FixedMesh. The input
   file must be an ASCII file formatted as a series of entries

   R1   v1
   R2   v2
   R3   v3
   ...

   where the radii are in kpc and velocities are in km/s. Blank lines
   are fine, and any lines starting with # are treated as comments and
   ignored.

   The code constructs a smooth representation of the input rotation
   curve by performing a quintic B-spline fit to the input data. This
   guarantees that the 2nd derivative of the rotation curve is
   computed with high accuracy, which is necessary to ensure that the
   simulation doesn't run into trouble.
 */

class FixedMeshTabRotation : public FixedMesh {
 public:
  FixedMeshTabRotation(std::string,Dimensions *,double,double,unsigned int);
  ~FixedMeshTabRotation();
  double beta(double x);
  double betap(double x);
  double uu(double x);
  void dumpRotCurve(std::string, Dimensions *);
 private:
  unsigned int nEntries; // Number of entries in tabulated rotation curve
  double *rInput, *vInput; // Input radius vs. velocity (dimensional)
  gsl_vector *coef; // Coefficients of basis splines
  gsl_vector *B;    // Values of basis splines
  gsl_matrix *dB;   // Values of basis splines and their derivatives
  gsl_matrix *cov;  // Covariance matrix
  gsl_bspline_workspace *wksp; // Workspace
  gsl_bspline_deriv_workspace *dwksp; // More workspace
  double xlast;
};
